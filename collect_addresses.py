from os.path import exists
from sqlite3 import connect, IntegrityError
from time import time

from yellowpages_locu import locu_addresses, yellow_addresses

DB_FILENAME = 'random_addresses.db'


def create_db():
    try:
        db = connect(DB_FILENAME)
        cursor = db.cursor()
        cursor.execute('''
            CREATE TABLE IF NOT EXISTS
            addresses(id INTEGER PRIMARY KEY, address TEXT UNIQUE,
            city TEXT, province TEXT, postal_code TEXT,
            latitude TEXT, longitude TEXT)'''
                       )
        db.commit()
    except Exception as e:
        db.rollback()
        raise e
    finally:
        db.close()


def insert_rows(rows):
    try:
        db = connect(DB_FILENAME)
        cursor = db.cursor()
        for row in rows:
            try:
                cursor.execute('''
                    INSERT INTO addresses(address, city, province,
                    postal_code, latitude, longitude)
                    VALUES(?, ?, ?, ?, ?, ?)''', row)
            except IntegrityError:
                pass
        db.commit()
    except Exception as e:
        db.rollback()
        raise e
    finally:
        db.close()


def check_db(city):
    try:
        db = connect(DB_FILENAME)
        cursor = db.cursor()
        cursor.execute('SELECT * FROM addresses WHERE city=?', (city,))
        matching_addresses = cursor.fetchall()
    except Exception as e:
        db.rollback()
        raise e
    finally:
        db.close()
    return matching_addresses


if __name__ == '__main__':
    if not exists(DB_FILENAME):
        create_db()

    # check for sys.argv input first??
    city = input('Enter city: ').strip()

    # check for matches inside database first
    from_db = check_db(city)
    if from_db:
        for row in sorted(from_db, key=lambda a: a[1]):
            print(row[1])
    else:
        start = time()
        insert_rows(locu_addresses(city) + yellow_addresses(city))
        print('Elapsed Time: {:.3f} seconds'.format(time() - start))
