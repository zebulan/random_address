from functools import reduce
from random import randint
from requests import get

from api_keys import LOCU_API_KEY, YELLOWPAGES_API_KEY

LOCU_KEYS = ('street_address', 'locality', 'region',
             'postal_code', 'lat', 'long')
LOCU_URL = 'https://api.locu.com/v1_0/venue/search/'
TO_REPLACE = (('Northwest', 'NW'), ('Northeast', 'NE'),
              ('Southeast', 'SE'), ('Southwest', 'SW'), ('Drive', 'Dr'),
              ('Trail', 'Trl'), ('Landng', 'Landing'), ('Street', 'St'),
              (' - ', '-'), ('.', ''), (',', ''))
YELLOW_URL = 'http://api.sandbox.yellowapi.com/FindBusiness/'


# use re.sub() instead?
# http://stackoverflow.com/questions/6116978/python-replace-multiple-strings

# import re
#
# rep = {"condition1": "", "condition2": "text"} # desired replacements here
#
# # use these three lines to do the replacement
# rep = dict((re.escape(k), v) for k, v in rep.iteritems())
# pattern = re.compile("|".join(rep.keys()))
# text = pattern.sub(lambda m: rep[re.escape(m.group(0))], text)

# # -------------------------------------------------------------
# pattern = re.compile("|".join(re.escape(k) for k in rep))
# text = pattern.sub(lambda m: rep[m.group(0)], text)


def clean_street_address(address):
    return reduce(lambda a, kv: a.replace(*kv), TO_REPLACE, address).strip()


def locu_addresses(locality=''):
    payload = {
        'api_key': LOCU_API_KEY,
        # 'location': '{}, {}'.format(latitude, longitude),
        'locality': locality
    }

    r = get(LOCU_URL, params=payload)
    if r.status_code != 200:
        print('Locu - Response #: {}'.format(r.status_code))
        return []

    addresses = []
    for i, address in enumerate(r.json()['objects'], start=1):
        is_valid = True
        valid_address = []
        for key in LOCU_KEYS:
            current = address[key]
            if not current:
                is_valid = False
                break
            valid_address.append(current)
        if is_valid:
            valid_address[0] = clean_street_address(valid_address[0])
            addresses.append(valid_address)
    return addresses


def yellow_addresses(locality=''):
    """ SANDBOX API: 300 calls per day, 1 call per second MAXIMUM """
    payload = {
        # pg = 1 - 50        # requested page
        'what': 'business',  # keyword, business name or phone number
        'where': locality,
        # 'where': 'cZ51.03977045017534,-114.07616310273404',
        'pgLen': 100,        # total results per page, integer 1 - 100
        # 'dist' = 1         # positive decimal value | max distance within KM
        'fmt': 'JSON',       # output format, XML or JSON
        'apikey': YELLOWPAGES_API_KEY,
        'UID': randint(1, 4097)
    }
    r = get(YELLOW_URL, params=payload)
    if r.status_code != 200:
        print('Yellow - Response #: {}'.format(r.status_code))
        return []

    return [(
        clean_street_address(a['address']['street']),
        a['address']['city'],
        a['address']['prov'],
        a['address']['pcode'],
        a['geoCode']['latitude'],
        a['geoCode']['longitude']
    ) for a in r.json()['listings']]
